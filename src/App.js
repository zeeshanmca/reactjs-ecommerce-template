/*
 * @Author: Zeeshan  Ahmad
 * @Date:   2020-09-16 14:47:37
 * @Last Modified by:   Zeeshan  Ahmad
 * @Last Modified time: 2020-09-16 15:04:11
 */
import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/Navbar';

function App() {
  return (
    <>

      <NavBar />
    </>
  );
}

export default App;
