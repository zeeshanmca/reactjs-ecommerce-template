/*
 * @Author: Zeeshan  Ahmad
 * @Date:   2020-09-16 14:44:55
 * @Last Modified by:   Zeeshan  Ahmad
 * @Last Modified time: 2020-09-16 20:11:20
 */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';


ReactDOM.render(
  <>
    <App />
  </>,
  document.getElementById('root')
);



