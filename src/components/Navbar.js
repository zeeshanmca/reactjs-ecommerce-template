/*
 * @Author: Zeeshan  Ahmad
 * @Date:   2020-09-16 14:50:28
 * @Last Modified by:   Zeeshan  Ahmad
 * @Last Modified time: 2020-09-16 20:52:38
 */


'use strict';
import 'bootstrap/dist/css/bootstrap.css';

import React from 'react'
import './navbar.css';




function Navbar() {
    return (
        <>
            <div className="container">
                <nav className="navbar navbar-expand-lg  navbar-light">

                    <button className="navbar-toggler btn btn-success" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <a className="navbar-brand" href="#">BookWorld</a>
                        <ul className="navbar-nav mx-auto mr-3  ml-3 mt-2 mr-5 mt-lg-0">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Home </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Courses</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">About US</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Contact</a>
                            </li>

                        </ul>

                    </div>
                </nav>
            </div>

        </>
    );
}

export default Navbar;